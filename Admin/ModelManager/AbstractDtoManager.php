<?php declare(strict_types=1);

namespace SymfonyBro\SonataThings\Admin\ModelManager;


use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;

/**
 * Class AbstractDtoManager
 */
abstract class AbstractDtoManager extends ModelManager
{

    /**
     * @return mixed
     */
    abstract protected function getSubjectClass();

    /**
     * @param mixed $object
     *
     * @return void
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function create($object): void
    {
        $instance = $this->doCreate($object);

        $this->getEntityManager($object)->persist($instance);
        $this->getEntityManager($object)->flush();

        $object->id = $instance->getId();
    }

    /**
     * @param $object
     *
     * @return object
     */
    abstract protected function doCreate($object);

    /**
     * @param string $class
     *
     * @return object
     */
    public function getModelInstance($class)
    {
        return $this->doGetModelInstance($class);
    }

    /**
     * @param $class
     *
     * @return object
     */
    abstract protected function doGetModelInstance($class);

    /**
     * @param string $class
     *
     * @return array
     */
    public function getExportFields($class): array
    {
        $metadata = $this->getEntityManager($this->getSubjectClass())->getClassMetadata($this->getSubjectClass());

        return $metadata->getFieldNames();
    }

    /**
     * @param string $class
     *
     * @return ObjectManager
     */
    public function getEntityManager($class): ObjectManager
    {
        return parent::getEntityManager($this->getSubjectClass());
    }

    /**
     * @param string $class
     *
     * @return ClassMetadata
     */
    public function getMetadata($class): ClassMetadata
    {
        return parent::getMetadata($this->getSubjectClass());
    }

    /**
     * @param string $baseClass
     * @param string $propertyFullName
     *
     * @return array
     */
    public function getParentMetadataForProperty($baseClass, $propertyFullName): array
    {
        return parent::getParentMetadataForProperty($this->getSubjectClass(), $propertyFullName);
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function hasMetadata($class): bool
    {
        return $this->getEntityManager($this->getSubjectClass())->getMetadataFactory()->hasMetadataFor($this->getSubjectClass());
    }

    /**
     * @param string $class
     * @param string $alias
     *
     * @return ProxyQuery
     * @throws \LogicException
     */
    public function createQuery($class, $alias = 'o'): ProxyQuery
    {
        $repository = $this
            ->getEntityManager($this->getSubjectClass())
            ->getRepository($this->getSubjectClass());

        if (!$repository instanceof EntityRepository) {
            throw new \LogicException('AbstractDtoManager works with EntityManager only');
        }

        return new ProxyQuery($repository->createQueryBuilder($alias));
    }

    /**
     * @param string $class
     * @param mixed $id
     *
     * @return null|object
     * @throws \LogicException
     */
    public function find($class, $id)
    {
        if (empty($id)) {
            return null;
        }
        $values = \array_combine($this->getIdentifierFieldNames($class), \explode(self::ID_SEPARATOR, $id));

        return $this->getRepository()->find($values);
    }

    /**
     * {@inheritdoc}
     * @throws \LogicException
     */
    public function findBy($class, array $criteria = []): array
    {
        return $this->getRepository()->findBy($criteria);
    }

    /**
     * {@inheritdoc}
     * @throws \LogicException
     */
    public function findOneBy($class, array $criteria = [])
    {
        return $this->getRepository()->findOneBy($criteria);
    }

    /**
     * @return EntityRepository
     * @throws \LogicException
     */
    private function getRepository(): EntityRepository
    {
        $repository = $this
            ->getEntityManager($this->getSubjectClass())
            ->getRepository($this->getSubjectClass());

        if (!$repository instanceof EntityRepository) {
            throw new \LogicException('AbstractDtoManager works with EntityManager only');
        }

        return $repository;
    }

    /**
     * @param object $entity
     *
     * @return mixed
     * @throws \RuntimeException
     */
    public function getNormalizedIdentifier($entity)
    {
        if (is_scalar($entity)) {
            throw new \RuntimeException('Invalid argument, object or null required');
        }

        if (!$entity || \is_array($entity) || null === $entity->getId()) {
            return;
        }

        $values = $this->doGetIdentifierValues($entity);

        if (0 === \count($values)) {
            return;
        }

        return implode(self::ID_SEPARATOR, $values);
    }

    /**
     * @param object $entity
     * @return array
     */
    protected function doGetIdentifierValues($entity): array
    {
        return [$entity->getId()];
    }
}
